package Sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindrome() {
		assertTrue("Invalid value for palindrome", Palindrome.isPalindrome( "anna" ));
	}
	
	@Test
	public void testIsPalindromeNegative() {
		assertFalse("Invalid value for palindrome", Palindrome.isPalindrome( "Anna has a lamb" ));
	}
	
	@Test
	public void testIsPalindromeBoundaryIn () {
		assertTrue("Invalid value for palindrome", Palindrome.isPalindrome("Aa"));
	}
	
	@Test
	public void testIsPalindromeBoundaryOut () {
		assertFalse("Invalid value for palindrome", Palindrome.isPalindrome("racer car"));
	}

}
